<?php

namespace Drupal\currency\Controller;

use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Created by PhpStorm.
 * User: kasper
 * Date: 1/31/17
 * Time: 11:11 AM
 */
class CurrencyController {

    public function index() {

        $result = \Drupal::entityQuery('taxonomy_term')->condition('vid', 'currency')->sort('tid', 'DESC')->range(0, 2)->execute();

        if (!empty($result)) {
            $tid = array_shift($result);
            $term = Term::load($tid);

            if (count($result)) {
                $tid = array_shift($result);
                $old = Term::load($tid);
                foreach (currency_fields() as $field_name => $currency_field) {
                    $value = $term->get($field_name)->getValue()[0]['value'];
                    $old_value = $old->get($field_name)->getValue()[0]['value'];
                    if ($old_value < $value ) {
                        $status[] = 'up';
                    }
                    elseif ($old_value > $value ) {
                        $status[] = 'down';
                    }
                    else {
                        $status[] = 'none';
                    }
                }
            }
            else {
                $status = [
                    'none',
                    'none',
                    'none',
                    'none',
                    'none',
                    'none',
                ];
            }

            return new JsonResponse([
                'buy' => [
                    'AED' => ['value' => $term->get('field_buy_aed')->getValue()[0]['value'], 'status' => array_shift($status)],
                    'EUR' => ['value' => $term->get('field_buy_eur')->getValue()[0]['value'], 'status' => array_shift($status)],
                    'USD' => ['value' => $term->get('field_buy_usd')->getValue()[0]['value'], 'status' => array_shift($status)],
                ],
                'sell' => [
                    'AED' => ['value' => $term->get('field_sell_aed')->getValue()[0]['value'], 'status' => array_shift($status)],
                    'EUR' => ['value' => $term->get('field_sell_eur')->getValue()[0]['value'], 'status' => array_shift($status)],
                    'USD' => ['value' => $term->get('field_sell_usd')->getValue()[0]['value'], 'status' => array_shift($status)],
                ],
                'time' => $term->getChangedTime(),
            ]);
        }

        return new JsonResponse([]);
    }

}
# Currency

<h3> Installation </h3>

Download this module and add in module directory and enable it.

<h3> How to work? </h3>
This module create a vocabulary that named <code>currency</code>.<br>
This module get data from <code> http://www.tgju.org/?act=sanarateservice&client=tgju&noview&type=json</code> and create currency term every run cron.

<b>Fields:</b>
 <br><code> field_buy_aed </code>
 <br><code> field_buy_eur </code>
 <br><code> field_buy_usd </code>
 <br><code> field_sell_aed </code>
 <br><code> field_sell_eur </code>
 <br><code> field_sell_usd </code>

<b>Route request:</b>
 <br>
 <code> api/currency </code>

<b>Response:</b>
 <br><code> buy </code> 
 <br><code> sell </code>